from django.shortcuts import render

# importing HttResponse from library
from django.http import HttpResponse
 
def home(request):
    # request is handled using HttpResponse object
    # return HttpResponse("Any kind of HTML Here")
    
    
    return render(request,"core/home.html")